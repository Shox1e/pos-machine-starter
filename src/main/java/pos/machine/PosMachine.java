package pos.machine;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public static String printReceipt(List<String> barcodes) {
        List<Item> loadAllItems = ItemsLoader.loadAllItems();
        Map<Item, Integer> receiptInfos = new HashMap<>();
        Integer total=0;
        StringBuilder result = new StringBuilder();

        saveBarcodesInfo(barcodes, loadAllItems, receiptInfos);

        printReceiptInfo(receiptInfos, total, result);
        return String.valueOf(result);
    }

    private static void printReceiptInfo(Map<Item, Integer> receiptInfos, Integer total, StringBuilder result) {
        result.append("***<store earning no money>Receipt***\n");
        for (Map.Entry<Item, Integer> receiptInfo : receiptInfos.entrySet()) {
            result.append("Name: ").append(receiptInfo.getKey().getName()).append(",").append(" Quantity: ").append(receiptInfo.getValue()).append(",").append(" Unit price: ")
                    .append(receiptInfo.getKey().getPrice()).append(" (yuan)").append(",").append(" Subtotal: ").append(countSubtotal(receiptInfo)).append(" (yuan)").append("\n");
            total = countTotal(total, receiptInfo);

        }
        result.append("----------------------\n").append("Total: ").append(total).append(" (yuan)\n").append("**********************");
    }

    private static void saveBarcodesInfo(List<String> barcodes, List<Item> loadAllItems, Map<Item, Integer> receiptInfos) {
        for (String barcode : barcodes) {
            for (Item item : loadAllItems) {
                if (isValid(barcode, item)) {
                    receiptInfos.put(item, receiptInfos.getOrDefault(item, 0) + 1);
                }
            }
        }
    }

    private static boolean isValid(String barcode, Item item) {
        return item.getBarcode().equals(barcode);
    }

    private static Integer countTotal(Integer total, Map.Entry<Item, Integer> receiptInfo) {
        total += countSubtotal(receiptInfo);
        return total;
    }

    private static int countSubtotal(Map.Entry<Item, Integer> receiptInfo) {
        return receiptInfo.getKey().getPrice() * receiptInfo.getValue();
    }
}
