**O：**Today, I learned about the use and submission specifications of ContextMap and Git, which greatly helped me understand the requirements and handle Git submission specifications. At the same time, I also learned about the naming conventions of code, which greatly helped me understand the code specifications. In the afternoon, I also participated in relevant exercises on ContextMap. 

**R：**I feel very happy about today's class and can make progress faster. 

**I：**What impressed me the most was the first practice, which explained my understanding in front of the whole class and communicated with everyone, which left a deep impression on me. 

**D：**What I gained the most today is my ability to break down requirements and tasks, which better enables me to write code in a standardized manner. These abilities will play a very important role in my daily work in the future.